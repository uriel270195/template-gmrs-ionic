import { NgModule } from '@angular/core';
import { PrincipalHeaderComponent } from 'src/app/commons/components/principal-header/principal-header.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [CommonModule, IonicModule],
  declarations: [PrincipalHeaderComponent],
  exports: [PrincipalHeaderComponent]
})
export class ComponentsModule {}
