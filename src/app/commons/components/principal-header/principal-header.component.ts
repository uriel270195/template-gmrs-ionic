import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-principal-header',
  templateUrl: './principal-header.component.html'
})
export class PrincipalHeaderComponent implements OnInit {

  @Input() tittle: string;

  constructor() { }

  ngOnInit() {}

}
