import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PAGES } from '../pages';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  login: LoginI = {};

  constructor(private alertController: AlertController, private router: Router) { }

  ngOnInit() {
  }
  processForm() {
    this.alertController.create({
      header: 'Login Success',
      message: `Welcome: <b>${this.login.userName}</b>`,
      buttons: [{
        text: 'OK',
        handler: () => {
          this.router.navigate([PAGES.HOME], {replaceUrl: true});
        }
      }]
    }).then(alert => alert.present());
  }

}

interface LoginI {
  userName?: string;
  password?: string;
}
