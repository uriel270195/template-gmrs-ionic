export const PAGES = {
    LOGIN: 'login',
    HOME: 'principal/home/Inbox',
    FILES: '/principal/files',
    COMPONENTS: {
        BADGES: '/principal/badges'
    }
};
