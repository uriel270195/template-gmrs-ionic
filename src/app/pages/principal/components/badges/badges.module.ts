import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BadgesPageRoutingModule } from './badges-routing.module';

import { BadgesPage } from './badges.page';
import { CommonsProyectModule } from 'src/app/commons/commons.module';

@NgModule({
  imports: [
    CommonModule,
    CommonsProyectModule,
    FormsModule,
    IonicModule,
    BadgesPageRoutingModule
  ],
  declarations: [BadgesPage]
})
export class BadgesPageModule {}
