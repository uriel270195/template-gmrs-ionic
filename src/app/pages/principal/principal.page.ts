import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PAGES } from '../pages';

@Component({
  selector: 'app-principal',
  templateUrl: 'principal.page.html',
  styleUrls: ['principal.page.scss'],
})
export class PrincipalPage {

  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Home',
      url: '/principal/home/Home',
      icon: 'home',
      action: () => {
      }
    },
    {
      title: 'Files',
      url: PAGES.FILES,
      icon: 'document',
      action: () => {
      }
    },
    {
      title: 'Favorites',
      url: '/principal/home/Favorites',
      icon: 'heart',
      action: () => {
      }
    },
    {
      title: 'Messages',
      url: '/principal/home/Messages',
      icon: 'mail',
      action: () => {
      }
    },
    {
      title: 'About',
      url: '/principal/home/About',
      icon: 'information-circle',
      action: () => {
      }
    },
    {
      title: 'Log out',
      icon: 'log-out',
      action: () => {
        this.router.navigate([PAGES.LOGIN]);
      }
    }
  ];
  public appComponents = [
    {
      title: 'Badges',
      url: PAGES.COMPONENTS.BADGES,
      icon: null,
      action: () => {
      }
    }
  ];

  constructor(private menuCtrl: MenuController, private router: Router) {}

  async openMenu() {
    await this.menuCtrl.open();
  }

}
